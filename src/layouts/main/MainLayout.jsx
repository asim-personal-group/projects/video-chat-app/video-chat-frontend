import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Route } from "react-router-dom";
import { Row, Col } from "reactstrap";

import { getUsersCollection } from "../../store/users";
import { getUserId, createUser } from "../../store/auth";
import { auth } from "../../services";

import { ContactList } from "../../components/common";

const MainLayout = ({ component: RouteComponent, ...rest }) => {
	let dispatch = useDispatch();
	const userCollection = useSelector(getUsersCollection());
	const userId = useSelector(getUserId());

	const [expanded, setExpanded] = useState(true);

	const toggleExpanded = () => setExpanded(!expanded);

	useEffect(() => {
		if (localStorage.getItem("LOGGED_IN")) {
			auth.getUserFromLocalStorage().then((userData) => {
				dispatch(createUser(userData));
			});
		}
	}, []);
	return (
		<Route
			render={(componentProps) => (
				<Row className="main-content-height">
					<Col xs={expanded ? 2 : 1} className="bg-dark shadow-lg main-content-overflow p-0">
						<ContactList users={userCollection} currentUser={userId} toggle={toggleExpanded} expanded={expanded} />
					</Col>
					<Col className="main-content-overflow p-0">
						<RouteComponent {...componentProps} />
					</Col>
				</Row>
			)}
			{...rest}
		/>
	);
};

export default MainLayout;
