import React from "react";
import { Redirect, Switch, BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import { Container } from "reactstrap";
import { ToastContainer } from "react-toastify";

import MainLayout from "./layouts";
import Home, { NotFound } from "./components";

import { getStore } from "./store";

const store = getStore();

const App = (_) => {
	return (
		<Provider store={store}>
			<ToastContainer autoClose={3000} />
			<BrowserRouter>
				<Container fluid>
					<Switch>
						<MainLayout exact path="/" component={Home} />
						<MainLayout path="/not-found" component={NotFound} />
						<Redirect to="/not-found" />
					</Switch>
				</Container>
			</BrowserRouter>
		</Provider>
	);
};

export default App;
