import { socket } from "./socket";
/**
 *  TO EMIT
 *      call-user
 *      cancel-call
 *      accept-call
 *      end-call
 *      reject-call
 *      join-room // multi user call
 */
export const commEmmitter = {
	emitCallUser: ({ caller, callee, signal }) => {
		console.log("emit call");
		socket.emit("call-user", { caller, callee, signal });
	},
	emitCancelCall: ({ caller, callee }) => {
		console.log("emit cancel");
		socket.emit("cancel-call", { caller, callee });
	},
	emitAcceptCall: ({ caller, callee, signal }) => {
		console.log("emit accept");
		socket.emit("accept-call", { caller, callee, signal });
	},
	emitEndCall: ({ peer, user }) => {
		console.log("emit end");
		socket.emit("end-call", { peer, user });
	},
	emitRejectCall: ({ caller, callee }) => {
		console.log("emit reject");
		socket.emit("reject-call", { caller, callee });
	},
};
