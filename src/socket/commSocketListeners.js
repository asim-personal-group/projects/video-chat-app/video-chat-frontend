import { toast } from "react-toastify";
import { getUserCollection } from "../store/users";
import { receiveCall, callAccepted, callEnded } from "../store/communication";
/**
 *  TO LISTEN
 *      user-change
 *      incoming-call
 *      accepted
 *      ended
 *      rejected
 *      call-error
 */

// user connected or disconnected
const handleUserChange = (users, dispatch) => {
	try {
		dispatch(getUserCollection({ collection: users }));
	} catch (err) {
		console.error(err);
	}
};

// somebody is calling
const handleIncomingCall = (data, dispatch) => {
	try {
		console.log("listen incoming");
		let { caller, signal } = data;
		dispatch(receiveCall({ peerId: caller.id, peerSignal: signal }));
	} catch (err) {
		console.error(err);
	}
};

// callee accepted your call
const handleAcceptedCall = (data, dispatch) => {
	try {
		console.log("listen accepted");
		let { signal } = data;
		dispatch(callAccepted({ calleeSignal: signal }));
	} catch (err) {
		console.error(err);
	}
};

// caller or callee ended call
const handleEndedCall = (data, dispatch) => {
	try {
		console.log("listen ended");
		// no data
		dispatch(callEnded());
	} catch (err) {
		console.error(err);
	}
};

// callee rejected your call
const handleRejectedCall = (data, dispatch) => {
	try {
		console.log("listen rejected");
		// no data
		dispatch(callEnded());
	} catch (err) {
		console.error(err);
	}
};

// caller cancelled the call
const handleCancelledCall = (data, dispatch) => {
	try {
		console.log("listen cancelled");
		dispatch(callEnded());
	} catch (err) {
		console.error(err);
	}
};

// callee busy or offline or === caller
const handleCallError = (data, dispatch) => {
	try {
		// let { error } = data;
		console.log("listen error");
		toast.error(data.error);
		dispatch(callEnded());
	} catch (err) {
		console.error(err);
	}
};

export const commSocketListeners = (socket, dispatch) => {
	if (socket) {
		socket.on("user-change", (data) => handleUserChange(data, dispatch));
		socket.on("incoming-call", (data) => handleIncomingCall(data, dispatch));
		socket.on("accepted", (data) => handleAcceptedCall(data, dispatch));
		socket.on("ended", () => handleEndedCall(null, dispatch));
		socket.on("rejected", () => handleRejectedCall(null, dispatch));
		socket.on("cancelled", () => handleCancelledCall(null, dispatch));
		socket.on("call-error", (data) => handleCallError(data, dispatch));
		/* request-to-join => {
			state -> newUserIncoming = true
			show popup asking for request
			Yes -> emit to room -> allow user with ID
			state -> newUserIncoming = false
		}
		// */
	}
};
