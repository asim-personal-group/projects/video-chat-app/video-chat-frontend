import io from "socket.io-client";
import { commSocketListeners } from "./commSocketListeners";

export let socket = undefined;
export function socketInit(id, dispatch) {
	socket = io.connect(process.env.REACT_APP_URL_3TC_BACKEND, { auth: { id } });
	commSocketListeners(socket, dispatch);
	return socket;
}
