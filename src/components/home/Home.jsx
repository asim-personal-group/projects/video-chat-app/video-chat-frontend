import React, { Fragment, useEffect, useState, useRef } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Row, Col, CardBody } from "reactstrap";
import Peer from "simple-peer";

import HomeHeader from "./HomeHeader";
import { CallNotificationModal, VideoBox } from "../common";
import { commEmmitter } from "../../socket/commSocketEmitters";
import { userAuthenticated, getUserId } from "../../store/auth";
//	COMM ACTIONS
import {
	// initiateCall,
	sendCall,
	// receiveCall,
	// answerCall,
	acceptCall,
	// declineCall,
	rejectCall,
	// inCall,
	// cancelCall,
	// endCall,
	// callAccepted,
	// callRejected,
	// callError,
	ended,
	// handleCancelCall,
	// handleRejectCall,
} from "../../store/communication";

// COMM SELECTORS
import {
	getIsInitiatingCall,
	getIsSendingCall,
	getIsReceivingCall,
	getIsAnsweringCall,
	getIsRejectingCall,
	getIsCancellingCall,
	getIsEndingCall,
	getIsInCall,
	getIsEndedCall,
	getPeerId,
	getPeerSignal,
	// getCallType,
} from "../../store/communication";

import { constants } from "../../assets/js";

const { emitAcceptCall, emitCallUser, emitCancelCall, emitEndCall, emitRejectCall } = commEmmitter;

const Home = () => {
	const dispatch = useDispatch();
	const isAuthenticated = useSelector(userAuthenticated());
	const userId = useSelector(getUserId());
	const [userStream, setUserStream] = useState();
	const userPeer = useRef();
	const userVideo = useRef();
	const otherVideo = useRef();

	const isInitiatingCall = useSelector(getIsInitiatingCall());
	const isEndingCall = useSelector(getIsEndingCall());
	const isAnsweringCall = useSelector(getIsAnsweringCall());
	const isCancellingCall = useSelector(getIsCancellingCall());
	const isRejectingCall = useSelector(getIsRejectingCall());
	const isEndedCall = useSelector(getIsEndedCall());

	const isReceivingCall = useSelector(getIsReceivingCall());
	const isSendingCall = useSelector(getIsSendingCall());
	const isInCall = useSelector(getIsInCall());
	const peerId = useSelector(getPeerId());
	const peerSignal = useSelector(getPeerSignal());
	const [streamScreen, setScreenStream] = useState(false);
	const constraints = {
		// audio: false,
		audio: { echoCancellation: true, sampleSize: 1 },
		video: { width: 1280, height: 720, aspectRatio: 1.777777778 },
	};

	// TODO: can move handleInitiateCall & handleAnswerCall into 1
	const handleInitiateCall = () => {
		console.log("Starting call");
		if (isInitiatingCall) {
			navigator.mediaDevices
				.getUserMedia(constraints)
				.then((stream) => {
					console.log("SEND: Got Stream");
					setUserStream(stream);
					if (userVideo.current) {
						userVideo.current.srcObject = stream;
					}
					const peer = new Peer({
						initiator: true,
						trickle: false,
						config: { iceServers: constants.iceServers },
						iceTransportPolicy: "relay",
						reconnectTimer: 3000,
						stream: stream,
					});
					userPeer.current = peer;
					peer.on("signal", (userSignal) => {
						console.log("SEND: Got own video signal");
						emitCallUser({ caller: userId, callee: peerId, signal: userSignal });
						dispatch(sendCall());
					});
					peer.on("stream", (otherStream) => {
						console.log("SEND: Got other user stream");
						if (otherVideo.current) {
							otherVideo.current.srcObject = otherStream;
						}
					});
					peer.on("error", (err) => {
						console.log("SEND: some error occurred", err);
						handleEndCall();
					});
				})
				.catch((err) => {
					console.error(err);
				});
		}
	};

	const handleAnswerCall = () => {
		if (isAnsweringCall) {
			console.log("REC: Answering call");
			navigator.mediaDevices
				.getUserMedia(constraints)
				.then((stream) => {
					console.log("REC: Got stream");
					setUserStream(stream);
					if (userVideo.current) {
						userVideo.current.srcObject = stream;
					}
					const peer = new Peer({
						initiator: false,
						trickle: false,
						//
						stream: stream,
					});
					userPeer.current = peer;
					peer.on("signal", (userSignal) => {
						console.log("REC: Got own video signal");
						if (!isInCall) {
							emitAcceptCall({ signal: userSignal, caller: peerId, callee: userId });
							dispatch(acceptCall());
						}
					});
					peer.on("stream", (otherStream) => {
						console.log("REC: Got other user stream");
						if (otherVideo.current) {
							otherVideo.current.srcObject = otherStream;
						}
					});
					console.log("REC: sending signal to other user");
					peer.signal(peerSignal);
					peer.on("error", (err) => {
						console.log("REC: some error occurred", err);
						handleEndCall(err);
					});
				})
				.catch((err) => {
					console.error(err);
				});
		}
	};

	const handleCallStart = () => {
		console.log("IN CALL: Call started");
		if (isInCall) {
			if (userPeer.current) {
				console.log("IN CALL: got other user signal");
				userPeer.current.signal(peerSignal);
				// userPeer.current.on("stream", (otherStream) => {
				// 	if (otherVideo.current) {
				// 		otherVideo.current.srcObject = otherStream;
				// 	}
				// });
			}
		}
	};

	const handleCancelCall = () => {
		if (isCancellingCall) {
			cleanUp();
			emitCancelCall({ callee: peerId, caller: userId });
			dispatch(ended());
		}
	};

	const handleRejectCall = () => {
		if (isRejectingCall) {
			emitRejectCall({ caller: peerId, callee: userId });
			dispatch(rejectCall());
		}
	};

	const handleEndCall = (err) => {
		if (isEndingCall) {
			cleanUp();
			emitEndCall({ peer: peerId, user: userId });
			dispatch(ended());
		}
	};

	const handleEndedCall = () => {
		if (isEndedCall) {
			cleanUp();
			dispatch(ended());
		}
	};

	const cleanUp = () => {
		userVideo.current?.destroy?.();
		otherVideo.current?.destroy?.();
		userPeer.current?.destroy?.();
		userStream?.getTracks().forEach(function (track) {
			track.stop();
		});
		setUserStream(null);
	};

	const toggleScreenShare = () => {
		let streamScreenNow = !streamScreen;

		if (streamScreenNow) {
			navigator.mediaDevices.getDisplayMedia({ cursor: true }).then((screenStream) => {
				userPeer.current.replaceTrack(userStream.getVideoTracks()[0], screenStream.getVideoTracks()[0], userStream);
				userVideo.current.srcObject = screenStream;
				setScreenStream(streamScreenNow);
				screenStream.getTracks()[0].onended = () => {
					userPeer.current.replaceTrack(screenStream.getVideoTracks()[0], userStream.getVideoTracks()[0], userStream);
					userVideo.current.srcObject = userStream;
					classForUserVideo = "video-screenshare";
					setScreenStream(false);
				};
			});
		} else {
			// userStream.getTracks()[0].onended();
			navigator.mediaDevices.getUserMedia(constraints).then((camStream) => {
				userStream.getTracks()[0].stop();
				userPeer.current.replaceTrack(userStream.getVideoTracks()[0], camStream.getVideoTracks()[0], userStream);
				userVideo.current.srcObject = camStream;
			});
		}
	};

	useEffect(handleInitiateCall, [isInitiatingCall]);
	useEffect(handleAnswerCall, [isAnsweringCall]);
	useEffect(handleCallStart, [isInCall]);
	useEffect(handleEndCall, [isEndingCall]);
	useEffect(handleCancelCall, [isCancellingCall]);
	useEffect(handleRejectCall, [isRejectingCall]);
	useEffect(handleEndedCall, [isEndedCall]);
	let classForUserVideo = "video-user";
	return (
		<Fragment>
			<HomeHeader isAuthenticated={isAuthenticated} />
			<CardBody>
				<Row>
					<Col sm={isInCall ? 2 : 10}>
						<VideoBox
							classForUserVideo={classForUserVideo}
							videoRef={userVideo}
							mediaStream={userStream}
							controllable={true}
							isInCall={isInCall}
							isSendingCall={isSendingCall}
							isReceivingCall={isReceivingCall}
							muted={true}
							toggleScreenShare={toggleScreenShare}
							streamScreen={streamScreen}
						/>
					</Col>
					<Col sm={isInCall ? 10 : 2} style={{ display: isInCall ? "block" : "none" }}>
						<VideoBox videoRef={otherVideo} mediaStream={null} controllable={false} muted={false} />
					</Col>
				</Row>
			</CardBody>
			<CallNotificationModal />
		</Fragment>
	);
};

export default Home;
