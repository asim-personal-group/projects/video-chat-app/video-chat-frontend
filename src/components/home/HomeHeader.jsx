import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { CardHeader, Row, Col, Button, UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem, Spinner } from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUser } from "@fortawesome/free-solid-svg-icons";

// import { constants } from "../../assets/js";
import { createUser, getUsername, getUserId, userLoading, signOutUser } from "../../store/auth";
import { endCall } from "../../store/communication";
import { commEmmitter } from "../../socket/commSocketEmitters";
// const { rygpb } = constants;
const HomeHeader = ({ isAuthenticated }) => {
	const [username, setUsername] = useState("");
	const user = useSelector(getUsername());
	const id = useSelector(getUserId());
	const loading = useSelector(userLoading());
	const dispatch = useDispatch();

	const signOut = () => {
		dispatch(endCall());
		dispatch(signOutUser());
		commEmmitter.emitEndCall({ peer: null, user: id });
		window.location.reload();
	};

	return (
		<CardHeader>
			<Row>
				<Col xs={4}>Welcome to {process.env.REACT_APP_COMPANY} Video Chat</Col>
				{!isAuthenticated && (
					<Col>
						<input
							className="mr-2"
							type="text"
							placeholder="Enter username or ID"
							value={username}
							onChange={(e) => setUsername(e.target.value)}
						/>
						<Button outline color="primary" onClick={() => dispatch(createUser(username))} disabled={loading}>
							Join
						</Button>
					</Col>
				)}
				{isAuthenticated && (
					<Col className="text-right">
						<div className="d-flex float-right ">
							<FontAwesomeIcon className="mr-2 mt-1" icon={faUser} color="#585858" style={{ fontSize: 24 }} />
							<UncontrolledDropdown size="sm">
								<DropdownToggle caret>
									{!loading ? `${user}` : <Spinner className="ml-2" color="warning" type="grow" size="sm" />}
								</DropdownToggle>
								<DropdownMenu>
									<DropdownItem header className="text-monospace">
										{id}
									</DropdownItem>
									<DropdownItem divider></DropdownItem>
									<DropdownItem onClick={() => signOut()}>Sign Out</DropdownItem>
								</DropdownMenu>
							</UncontrolledDropdown>
						</div>
					</Col>
				)}
			</Row>
		</CardHeader>
	);
};

export default HomeHeader;
