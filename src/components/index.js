import Home from "./home/Home";
import NotFound from "./not-found/NotFound";

export default Home;
export { NotFound };
