import React from "react";
import { Row, Col } from "reactstrap";

const NotFound = (_) => {
	return (
		<Row className="justify-content-center mt-5">
			<Col lg={4} className="bg-danger rounded p-5 text-center text-white page-title">
				{`<404 /> Page Not Found`}
			</Col>
		</Row>
	);
};

export default NotFound;
