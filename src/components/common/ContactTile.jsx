import React from "react";
import { ListGroupItem, Row, Col } from "reactstrap";
import { faPhone, faVideo } from "@fortawesome/free-solid-svg-icons";

import { IconButton } from "./";

import { constants } from "../../assets/js";
const { rygpb } = constants;

const ContactTile = ({ username, id, disableButtons, func, ind }) => {
	return (
		<ListGroupItem style={{ backgroundColor: ind % 2 ? "#a0a0a055" : "#60606055", color: "#FFF" }}>
			<Row>
				<Col>
					{username} <span className="badge text-monospace">{id}</span>
				</Col>
				<Col className="text-right my-auto">
					<IconButton
						className="mr-2"
						size="sm"
						color={rygpb[4]}
						icon={faPhone}
						disable={disableButtons}
						onClick={() => func(id, "audio")}
					/>
					<IconButton
						className="mr-2"
						size="sm"
						color={rygpb[2]}
						icon={faVideo}
						disable={disableButtons}
						onClick={() => func(id, "video")}
					/>
				</Col>
			</Row>
		</ListGroupItem>
	);
};

export default ContactTile;
