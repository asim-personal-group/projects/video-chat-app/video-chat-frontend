import React, { Fragment, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Modal, ModalHeader, ModalBody, ModalFooter, Row, Col } from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUser, faTimes, faMicrophone, faVideo } from "@fortawesome/free-solid-svg-icons";
import { Howl } from "howler";

import { IconButton } from "./";

// import {
// 	getIsSendingCall,
// 	getIsReceivingCall,
// 	getPeerId,
// 	acceptCall,
// 	handleCancelCall as thunkHandleCancelCall,
// 	handleRejectCall,
// } from "../../store/communication";

// COMM SELECTORS
import { getIsReceivingCall, getIsSendingCall, getPeerId } from "../../store/communication";

// COMM ACTIONS
import { answerCall, declineCall, cancelCall } from "../../store/communication";

import { getUsersCollection } from "../../store/users";

import { ringtone } from "../../assets/audio";

const sound = new Howl({
	src: [ringtone],
	loop: true,
	volume: 0.5,
});
const CallNotificationModal = () => {
	const dispatch = useDispatch();
	const isReceivingCall = useSelector(getIsReceivingCall());
	const isSendingCall = useSelector(getIsSendingCall());
	const peerId = useSelector(getPeerId());
	const usersCollection = useSelector(getUsersCollection());
	let username = "";
	if (peerId) {
		username = usersCollection[peerId].username;
	}
	const isModalOpen = isReceivingCall || isSendingCall;

	const handleAcceptCall = (callType) => {
		dispatch(answerCall({ callType }));
	};

	const handleCancelCall = () => {
		isReceivingCall ? dispatch(declineCall()) : dispatch(cancelCall());
	};

	const toggleAudio = () => {
		if (isReceivingCall || isSendingCall) {
			sound.play();
		} else {
			sound.stop();
		}
	};

	useEffect(toggleAudio, [isReceivingCall, isSendingCall]);
	return (
		<Fragment>
			<Modal isOpen={isModalOpen} keyboard={false} centered>
				<ModalHeader>{isSendingCall ? `Calling...` : `Incoming Call...`}</ModalHeader>
				<ModalBody>
					<div className="text-center">
						<FontAwesomeIcon icon={faUser} style={{ fontSize: 36 }} color="#123456" />
					</div>
					<div className="text-center">{username}</div>
				</ModalBody>
				<ModalFooter>
					<Row className="text-center align-items-center" style={{ width: "100%" }}>
						{isReceivingCall && (
							<Col>
								<IconButton icon={faVideo} color="#28A745" onClick={() => handleAcceptCall("video")} />
							</Col>
						)}
						{isReceivingCall && (
							<Col>
								<IconButton icon={faMicrophone} color="#FFC107" onClick={() => handleAcceptCall("audio")} />
							</Col>
						)}
						<Col>
							<IconButton icon={faTimes} color="#DC3545" onClick={() => handleCancelCall()} />
						</Col>
					</Row>
				</ModalFooter>
			</Modal>
		</Fragment>
	);
};

export default CallNotificationModal;
