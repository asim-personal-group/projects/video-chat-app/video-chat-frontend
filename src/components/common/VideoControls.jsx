import React from "react";
import { useDispatch } from "react-redux";
import { faMicrophone, faMicrophoneSlash, faVideo, faVideoSlash, faPhoneSlash, faTv } from "@fortawesome/free-solid-svg-icons";

import { IconButton } from "./";

import { endCall } from "../../store/communication";
import { constants } from "../../assets/js";

const { rygpb } = constants;

const VideoControls = ({ video, audio, screen, handleControls }) => {
	const dispatch = useDispatch();
	const handleEndCall = () => {
		dispatch(endCall());
	};
	return (
		<div className="video-controls d-flex justify-content-between align-items-center">
			<IconButton icon={video ? faVideo : faVideoSlash} color={rygpb[2]} onClick={() => handleControls("video")} />
			<IconButton icon={audio ? faMicrophone : faMicrophoneSlash} color={rygpb[1]} onClick={() => handleControls("audio")} />
			<IconButton icon={faTv} color={rygpb[3]} onClick={() => handleControls("screen")} disabled={screen} />
			<IconButton icon={faPhoneSlash} color={rygpb[0]} onClick={handleEndCall} />
		</div>
	);
};

export default VideoControls;
