import React from "react";
import { Button } from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const IconButton = ({ icon, color, onClick, disable, size }) => {
	const circleButton = {
		width: "50px",
		height: "50px",
		padding: "6px 0px",
		borderRadius: "25px",
		textAlign: "center",
		fontSize: "22px",
		lineHeight: "1.42857",
	};

	return (
		<Button
			className="rsbutton btn btn-circle m-1"
			// size="sm"
			onClick={onClick}
			style={{ ...circleButton, backgroundColor: color, borderColor: "#00000000" }}
			disabled={disable}
		>
			<FontAwesomeIcon icon={icon} style={{ fontSize: circleButton.fontSize }} />
		</Button>
	);
};

export default IconButton;
