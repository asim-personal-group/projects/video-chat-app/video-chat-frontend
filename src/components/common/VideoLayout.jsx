import React, { Fragment } from "react";
import { Row, Col } from "reactstrap";

const VideoLayout = ({ userVideo, otherVideo, isInCall }) => {
	return (
		<Row>
			{!isInCall && (
				<Fragment>
					<Col sm={10}>{userVideo}</Col>
					<Col sm={2}>{otherVideo}</Col>
				</Fragment>
			)}

			{isInCall && (
				<Fragment>
					<Col sm={10}>{otherVideo}</Col>
					<Col sm={2}>{userVideo}</Col>
				</Fragment>
			)}
		</Row>
	);
	// !isInCall ? (
	// 	<Row>
	// 		<Col sm={10}>{userVideo}</Col>
	// 		<Col sm={2}>{otherVideo}</Col>
	// 	</Row>
	// ) : (
	// 	<Row>
	// 		<Col sm={10}>{otherVideo}</Col>
	// 		<Col sm={2}>{userVideo}</Col>
	// 	</Row>
	// );

	// // !streamScreen ? (
	// // 	<Row>
	// // 		<Col sm={10}>{otherVideo}</Col>
	// // 		<Col sm={2}>{userVideo}</Col>
	// // 	</Row>
	// // ) : (
	// // 	<Row>
	// // 		<Col sm={10}>{otherVideo}</Col>
	// // 		<Col sm={2}>
	// // 			<Row>{otherVideo}</Row>
	// // 			<Row>{userVideo}</Row>
	// // 		</Col>
	// // 	</Row>
	// // );
};

export default VideoLayout;
