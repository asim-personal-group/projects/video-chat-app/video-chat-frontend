import React, { Fragment } from "react";
import { useDispatch, useSelector } from "react-redux";
import { ListGroup, ListGroupItem, Row, Col, Button } from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUsers, faPhone, faVideo, faCaretLeft, faCaretRight } from "@fortawesome/free-solid-svg-icons";

import { initiateCall, getIsReceivingCall, getIsSendingCall, getIsInCall } from "../../store/communication";
import { constants } from "../../assets/js";
import { IconButton, ContactTile } from "./";
const { rygpb } = constants;

const ContactList = ({ users, currentUser, expanded, toggle }) => {
	const dispatch = useDispatch();
	const isReceivingCall = useSelector(getIsReceivingCall());
	const isSendingCall = useSelector(getIsSendingCall());
	const isInCall = useSelector(getIsInCall());

	const disableButtons = isReceivingCall || isSendingCall || isInCall;

	const callPeer = (peerId, callType) => {
		if (!disableButtons) {
			dispatch(initiateCall({ peerId, callType }));
		}
	};

	let userIdList = [];
	if (users) {
		userIdList = Object.keys(users).filter((uid) => uid !== currentUser && users[uid].socket !== null);
	}

	return (
		<Fragment>
			{userIdList && userIdList.length > 0 && (
				<div>
					<div className="mt-2 ml-4" style={{ color: "#fff" }}>
						<FontAwesomeIcon className="align-middle justify-content-center" icon={faUsers} />
						{expanded && <span className="ml-3 align-middle justify-content-center">Contacts</span>}
						{/* <Button
							className="bg-dark"
							style={{ position: "absolute", right: "0%", top: "0%", backgroundColor: "#00000000", borderColor: "#00000000" }}
							onClick={toggle}
						>
							<FontAwesomeIcon icon={expanded ? faCaretLeft : faCaretRight} color={rygpb[1]} style={{ fontSize: 36 }} />
						</Button> */}
					</div>
					<hr style={{ backgroundColor: rygpb[1] }} />
				</div>
			)}
			<ListGroup className="">
				{userIdList &&
					userIdList.length > 0 &&
					userIdList.map((uid, ind) => {
						return (
							<ContactTile
								key={`list_group_item_user_${ind}`}
								ind={ind}
								username={users[uid].username}
								id={users[uid].id}
								func={callPeer}
								disableButtons={disableButtons}
							/>
						);
					})}
			</ListGroup>
		</Fragment>
	);
};

export default ContactList;
