import React, { useState, useEffect, Fragment } from "react";
import { useSelector } from "react-redux";
import { Card } from "reactstrap";

import { VideoControls } from "./";
import { getCallType } from "../../store/communication";

const VideoBox = ({
	classForUserVideo,
	videoRef,
	mediaStream,
	controllable,
	isInCall,
	isSendingCall,
	isReceivingCall,
	muted,
	toggleScreenShare,
	streamScreen,
}) => {
	const callType = useSelector(getCallType());
	const [streamAudio, setStreamAudio] = useState(true);
	const [streamVideo, setStreamVideo] = useState(true);
	// const [streamScreen, setScreenStream] = useState(false);
	const toggleTracks = () => {
		if (mediaStream && controllable) {
			mediaStream.getTracks().forEach((track) => {
				let value = track.kind === "audio" ? streamAudio : streamVideo;
				track.enabled = value;
			});
		}
	};

	const handleControls = async (target) => {
		if (target === "audio" || target === "video") {
			target === "audio" ? setStreamAudio(!streamAudio) : setStreamVideo(!streamVideo);
		} else if (target === "screen") {
			// setScreenStream(!streamScreen);
			if (isInCall & !streamScreen) {
				toggleScreenShare();
			}
		}
	};

	useEffect(toggleTracks, [streamAudio, streamVideo, mediaStream, controllable]);
	useEffect(() => setStreamVideo(callType === "video" || callType === "" || callType === null), [callType]);
	useEffect(toggleTracks, []);
	return (
		<Fragment>
			<Card>
				<video
					className={streamScreen ? "video-screenshare" : classForUserVideo}
					ref={videoRef}
					muted={muted}
					playsInline
					autoPlay
				></video>
			</Card>
			{controllable && (isInCall || isSendingCall || isReceivingCall) && (
				<VideoControls
					className="p-0"
					audio={streamAudio}
					video={streamVideo}
					screen={streamScreen}
					handleControls={handleControls}
				/>
			)}
		</Fragment>
	);
};

export default VideoBox;
