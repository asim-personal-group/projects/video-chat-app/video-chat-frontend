import axios from "axios";

// const BASE_URL = `${process.env.REACT_APP_URL_3TC_BACKEND}${process.env.NODE_ENV === "production" ? "/nodejs/" : "/"}user`;
const BASE_URL = `${process.env.REACT_APP_URL_3TC_BACKEND}/user`;

const login = (username) => {
	let url = `${BASE_URL}/login`;
	if (localStorage.getItem("LOGGED_IN")) {
		return getUserFromLocalStorage();
	}
	return axios
		.post(url, { username })
		.then((res) => {
			setUserToLocalStorage(res.data);
			return res.data;
		})
		.catch((err) => {
			throw err;
		});
};

const getUserFromLocalStorage = () => {
	let userData = { id: localStorage.getItem("USER_ID"), username: localStorage.getItem("USERNAME") };
	return Promise.resolve(userData);
};

const setUserToLocalStorage = (data) => {
	localStorage.setItem("LOGGED_IN", true);
	localStorage.setItem("USER_ID", data.id);
	localStorage.setItem("USERNAME", data.username);
};

const auth = { login, getUserFromLocalStorage };

export default auth;
