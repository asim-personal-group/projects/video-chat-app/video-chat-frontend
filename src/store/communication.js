import { createSlice } from "@reduxjs/toolkit";
const getInitialState = (_) => ({
	isInitiatingCall: false,
	isSendingCall: false,
	isReceivingCall: false,
	isAnsweringCall: false,
	// hasAcceptedCall: false,
	isRejectingCall: false,
	// hasRejectedCall: false,
	isCancellingCall: false,
	isEndingCall: false,
	isInCall: false,
	isEndedCall: false,
	peerId: null,
	peerSignal: null,
	callType: "",
	// hasAcceptedCall: false,
	// hasRejectedCall: false,
	// hasEndedCall: false,
});

const CommunicationSlice = createSlice({
	name: "communication",
	initialState: getInitialState(),
	//  REDUCERS
	reducers: {
		initiateCall: (commSlice, action) => {
			commSlice.isInitiatingCall = true;
			commSlice.peerId = action.payload.peerId;
			commSlice.callType = action.payload.callType;
		},
		sendCall: (commSlice, action) => {
			commSlice.isInitiatingCall = false;
			commSlice.isSendingCall = true;
		},
		receiveCall: (commSlice, action) => {
			commSlice.isReceivingCall = true;
			commSlice.peerId = action.payload.peerId;
			commSlice.peerSignal = action.payload.peerSignal;
		},
		// accepting states
		// answer call is different from accept call because answer call allows to setup peer + video before emitting accept
		answerCall: (commSlice, action) => {
			commSlice.callType = action.payload.callType;
			commSlice.isReceivingCall = false;
			commSlice.isAnsweringCall = true;
		},
		acceptCall: (commSlice, action) => {
			commSlice.isAnsweringCall = false;
			commSlice.isInCall = true;
		},

		//rejecting states
		// decline is different from reject
		declineCall: (commSlice, action) => {
			commSlice.isReceivingCall = false;
			commSlice.isRejectingCall = true;
		},
		rejectCall: (commSlice, action) => {
			return getInitialState();
		},
		///---------------------------------------------------------------------------->>

		inCall: (commSlice, action) => {
			commSlice.isInCall = true;
		},
		cancelCall: (commSlice, action) => {
			commSlice.isSendingCall = false;
			commSlice.isCancellingCall = true;
		},
		endCall: (commSlice, action) => {
			commSlice.isInCall = false;
			commSlice.isEndingCall = true;
		},
		callAccepted: (commSlice, action) => {
			commSlice.peerSignal = action.payload.calleeSignal;
			commSlice.isSendingCall = false;
			commSlice.isInCall = true;
		},
		callEnded: (commSlice, action) => {
			commSlice.isInCall = false;
			commSlice.isReceivingCall = false;
			commSlice.isRejectingCall = false;
			commSlice.isEndingCall = false;
			commSlice.isEndedCall = true;
		},
		// callRejected: (commSlice, action) => {
		// 	return getInitialState();
		// },
		// callError: (commSlice, action) => {
		// 	return getInitialState();
		// },
		ended: (commSlice, action) => {
			return getInitialState();
		},

		changeCallType: (commSlice, action) => {
			commSlice.callType = action.payload.callType;
		},
	},
});
export default CommunicationSlice.reducer;

//  ACTIONS
const {
	initiateCall,
	sendCall,
	receiveCall,
	answerCall,
	acceptCall,
	declineCall,
	rejectCall,
	inCall,
	cancelCall,
	endCall,
	callAccepted,
	callEnded,
	// callRejected,
	// callError,
	ended,
	changeCallType,
} = CommunicationSlice.actions;

//  THUNK
// const handleCancelCall = () => (dispatch, getState) => {
// 	try {
// 		let peerId = getState().communication.peerId;
// 		socket.emit("end-call", { peer: peerId });
// 		dispatch(cancelCall());
// 	} catch (err) {}
// };

// const handleRejectCall = () => (dispatch, getState) => {
// 	try {
// 		let peerId = getState().communication.peerId;
// 		socket.emit("end-call", { peer: peerId });
// 		dispatch(rejectCall());
// 	} catch (err) {}
// };
export {
	initiateCall,
	sendCall,
	receiveCall,
	answerCall,
	acceptCall,
	declineCall,
	rejectCall,
	inCall,
	cancelCall,
	endCall,
	callAccepted,
	callEnded,
	// callRejected,
	// callError,
	ended,
	// handleCancelCall,
	// handleRejectCall,
	changeCallType,
};

//  SELECTORS
const getIsInitiatingCall = () => (state) => state.communication.isInitiatingCall;
const getIsSendingCall = () => (state) => state.communication.isSendingCall;
const getIsReceivingCall = () => (state) => state.communication.isReceivingCall;
const getIsAnsweringCall = () => (state) => state.communication.isAnsweringCall;
const getIsRejectingCall = () => (state) => state.communication.isRejectingCall;
const getIsCancellingCall = () => (state) => state.communication.isCancellingCall;
const getIsEndingCall = () => (state) => state.communication.isEndingCall;
const getIsInCall = () => (state) => state.communication.isInCall;
const getIsEndedCall = () => (state) => state.communication.isEndedCall;
const getPeerId = () => (state) => state.communication.peerId;
const getPeerSignal = () => (state) => state.communication.peerSignal;
const getCallType = () => (state) => state.communication.callType;

export {
	getIsInitiatingCall,
	getIsSendingCall,
	getIsReceivingCall,
	getIsAnsweringCall,
	getIsRejectingCall,
	getIsCancellingCall,
	getIsEndingCall,
	getIsInCall,
	getIsEndedCall,
	getPeerId,
	getPeerSignal,
	getCallType,
};
