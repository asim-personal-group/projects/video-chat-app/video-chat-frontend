import { createSlice } from "@reduxjs/toolkit";
const getInitialState = (_) => ({
	loading: false,
	collection: {},
});

const UsersSlice = createSlice({
	name: "users",
	initialState: getInitialState(),
	//  REDUCERS
	reducers: {
		requestUsers: (usersSlice, action) => {
			usersSlice.loading = true;
		},
		receiveUsers: (usersSlice, action) => {
			usersSlice.loading = false;
			usersSlice.collection = action.payload.collection;
		},
		receiveUsersFailure: (usersSlice, action) => {
			usersSlice.loading = false;
		},
	},
});
export default UsersSlice.reducer;

//  ACTIONS
const { requestUsers, receiveUsers, receiveUsersFailure } = UsersSlice.actions;

// THUNK
const getUserCollection = (collection) => (dispatch, getState) => {
	try {
		dispatch(requestUsers());
		dispatch(receiveUsers(collection));
	} catch (err) {
		dispatch(receiveUsersFailure(JSON.stringify(err)));
	}
};

export { getUserCollection };

//  SELECTORS

const isRequestingUsers = () => (state) => state.users.loading;
const getUsersCollection = () => (state) => state.users.collection;

export { isRequestingUsers, getUsersCollection };
