import {
	configureStore,
	// getDefaultMiddleware
} from "@reduxjs/toolkit";
import RootReducer from "./rootReducer";
// import { api, callbackOnAction } from "./middleware";

const getStore = (_) => {
	return configureStore({
		reducer: RootReducer,
		// middleware: [
		// 	...getDefaultMiddleware({ serializableCheck: { ignoredActions: ["api/callBegin"] } }),
		// 	api,
		// 	callbackOnAction
		// ]
	});
};

export { getStore };
