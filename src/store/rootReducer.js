import { combineReducers } from "@reduxjs/toolkit";
import AuthReducer from "./auth";
import UsersReducer from "./users";
import CommunicationReducer from "./communication";

const RootReducer = combineReducers({
	auth: AuthReducer,
	users: UsersReducer,
	communication: CommunicationReducer,
});

export default RootReducer;
