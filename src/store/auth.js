import { createSlice } from "@reduxjs/toolkit";
import { auth as authService } from "../services";
import { socketInit } from "../socket/socket";
// import connectSocket from "../utils/socket";

const getInitialState = (_) => ({
	loading: false,
	connecting: false,
	username: "",
	id: "",
	currentRoom: null,
	socket: null,
});

const AuthSlice = createSlice({
	name: "auth",
	initialState: getInitialState(),
	//  REDUCERS
	reducers: {
		creatingUser: (authSlice, action) => {
			authSlice.loading = true;
		},
		userCreated: (authSlice, action) => {
			authSlice.username = action.payload.username;
			authSlice.id = action.payload.id;
			authSlice.loading = false;
		},
		userCreationFailure: (authSlice, action) => {
			authSlice.loading = false;
		},
		connectingSocket: (authSlice, action) => {
			authSlice.connecting = true;
		},
		setSocket: (authSlice, action) => {
			authSlice.socket = action.payload.socket;
			authSlice.connecting = false;
		},
		connectionFailed: (authSlice, action) => {
			authSlice.connecting = false;
		},
		setCurrentRoom: (authSlice, action) => {
			authSlice.currentRoom = action.payload.currentRoom;
		},
		signingOut: (authSlice, action) => {
			authSlice.loading = true;
		},
		signOut: (authSlice, action) => {
			return getInitialState();
		},
		signOutFailure: (authSlice, action) => {
			authSlice.loading = false;
		},
	},
});
export default AuthSlice.reducer;

//  ACTIONS
const {
	creatingUser,
	userCreated,
	userCreationFailure,
	// setSocket,
	// setCurrentRoom,
	// connectingSocket,
	// connectionFailed
	signingOut,
	signOut,
	signOutFailure,
} = AuthSlice.actions;

//  THUNK
const createUser = (user) => async (dispatch, getState) => {
	dispatch(creatingUser());
	try {
		let userData = await authService.login(user);
		dispatch(userCreated(userData));
		socketInit(userData.id, dispatch);
		// dispatch(socketConnection());
	} catch (err) {
		dispatch(userCreationFailure(JSON.stringify(err)));
	}
};

// const socketConnection = () => (dispatch, getState) => {
// 	dispatch(connectingSocket());
// 	try {
// 		let {
// 			auth: { id },
// 		} = getState();
// 		let socket = connectSocket(id);
// 		dispatch(setSocket(socket));
// 	} catch (err) {
// 		dispatch(connectionFailed(JSON.stringify(err)));
// 	}
// };
const signOutUser = () => (dispatch, getState) => {
	try {
		dispatch(signingOut());
		dispatch(signOut());
		localStorage.clear();
	} catch (err) {
		dispatch(signOutFailure(JSON.stringify(err)));
	}
};

export { createUser, signOutUser };

//  SELECTORS
const userAuthenticated = () => (state) => !!state.auth.id;
const getUsername = () => (state) => state.auth.username;
const getUserId = () => (state) => state.auth.id;
const getUserCurrentRoom = () => (state) => state.auth.currentRoom;
const getUserSocket = () => (state) => state.auth.socket;
const userLoading = () => (state) => state.auth.loading;

export { userAuthenticated, getUsername, getUserId, getUserCurrentRoom, getUserSocket, userLoading };
