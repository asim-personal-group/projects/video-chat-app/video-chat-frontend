const constants = {
	iceServers: [
		{
			url: "turn:18.141.204.106:3478?transport=udp",
			username: "asim.iqbal",
			credential: "6dd57c4e4f153228",
		},
		// { url: "stun:stun01.sipphone.com" },
		// { url: "stun:stun.ekiga.net" },
		// { url: "stun:stun.fwdnet.net" },
		// { url: "stun:stun.ideasip.com" },
		// { url: "stun:stun.iptel.org" },
		// { url: "stun:stun.rixtelecom.se" },
		// { url: "stun:stun.schlund.de" },

		// { url: "stun:stun.l.google.com:19302" },
		// { url: "stun:stun1.l.google.com:19302" },
		// { url: "stun:stun2.l.google.com:19302" },
		// { url: "stun:stun3.l.google.com:19302" },
		// { url: "stun:stun4.l.google.com:19302" },
		// { urls: "stun:global.stun.twilio.com:3478?transport=udp" },

		// { url: "stun:stunserver.org" },
		// { url: "stun:stun.softjoys.com" },
		// { url: "stun:stun.voiparound.com" },
		// { url: "stun:stun.voipbuster.com" },
		// { url: "stun:stun.voipstunt.com" },
		// { url: "stun:stun.voxgratia.org" },
		// { url: "stun:stun.xten.com" },
		// {
		// 	url: "turn:numb.viagenie.ca",
		// 	credential: "muazkh",
		// 	username: "webrtc@live.com",
		// },
		// {
		// 	url: "turn:192.158.29.39:3478?transport=udp",
		// 	credential: "JZEOEt2V3Qb0y27GRntt2u2PAYA=",
		// 	username: "28224511:1379330808",
		// },
		// {
		// 	url: "turn:192.158.29.39:3478?transport=tcp",
		// 	credential: "JZEOEt2V3Qb0y27GRntt2u2PAYA=",
		// 	username: "28224511:1379330808",
		// },
	],
	rygpb: ["#D92550", "#F7B924", "#3AC47D", "#545CD8", "#00AAFF", "#798fa8"],

	pantone: [
		"#e34132",
		"#fd823e",
		"#ffaf12",
		"#b49c73",
		"#f0daa4",
		"#565d47",
		"#4ec5a5",
		"#117893",
		"#34558b",
		"#798fa8",
		"#72617d",
		"#a2553a",
		"#eaac9d",
		"#3b3d4b",
		"#a09d9c",
		"#eff0f1",
	],
};

export { constants };
